package com.harside.interfaces.ex1;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.View;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    List<Movie> latestMoviesList;
    RecyclerView rvLatestMovies;
    LinearLayoutManager llLatestMovies;
    MoviesAdapter moviesAdapter;

    RecyclerView rvRecommended;
    MoviesAdapter recommendedAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        initData();
        initUI();
    }

    private void initData() {

        latestMoviesList = new ArrayList<>();
        latestMoviesList.add(new Movie("The Avengers","http://es.web.img3.acsta.net/c_215_290/pictures/18/03/16/15/33/3988420.jpg"));
        latestMoviesList.add(new Movie("Incredible 2","http://es.web.img3.acsta.net/c_215_290/pictures/18/04/13/11/46/5802807.jpg"));
        latestMoviesList.add(new Movie("The Rogue","http://whatchareading.com/wp-content/uploads/2015/06/11238221_10150584945129980_6162443440606809085_o.jpg"));
        latestMoviesList.add(new Movie("Deadpool 2","http://es.web.img3.acsta.net/pictures/18/04/26/11/50/5029006.jpg"));



    }

    private void initUI() {

        rvLatestMovies = findViewById(R.id.rvLatestMovies);
        moviesAdapter = new MoviesAdapter(latestMoviesList);
        llLatestMovies = new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false);
        rvLatestMovies.setLayoutManager(llLatestMovies);
        rvLatestMovies.setAdapter(moviesAdapter);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
