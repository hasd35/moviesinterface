package com.harside.interfaces.ex1;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.net.URI;
import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder> {

    private static final String TAG = MoviesAdapter.class.getSimpleName();

    private List<Movie> listMovie;

    public MoviesAdapter(List<Movie> listMovie) {
        this.listMovie = listMovie;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_layout, parent, false);
        return new MovieViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        Movie movie = listMovie.get(position);
        if (movie != null) {
            Log.i(TAG, "onBindViewHolder: "+movie.getUrlImage());

            ;

            Picasso.get()
                    .load(movie.getUrlImage())
                    .error(R.drawable.no)
                    .into(holder.ivTitlePage);

        }

    }

    @Override
    public int getItemCount() {
        return listMovie!=null?listMovie.size():0;
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {
        ImageView ivTitlePage;

        MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            ivTitlePage = itemView.findViewById(R.id.ivTitlePage);

        }
    }
}
